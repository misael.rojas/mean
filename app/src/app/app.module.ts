import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { appRouting, appRoutingProviders } from './app.routing';

import { AppComponent } from './app.component';

import { SimpleTinyComponent } from './components/shared/simple-tiny/simple-tiny.component';

import { StoreComponent } from './components/store/store.component';
import { ParksComponent } from './components/parks/parks.component';
import { AnimalsComponent } from './components/animals/animals.component';
import { ContactComponent } from './components/contact/contact.component';
import { KeeperComponent } from './components/keeper/keeper.component';
import { HomeComponent } from './components/home/home.component';
import { NavComponent } from './components/shared/nav/nav.component';
import { ChurrosComponent } from './components/churros/churros.component';

// modules
import {EmailModule} from './email/email.module';
import { AdminModule } from './admin/admin.module';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';

// services 
import { UserService } from './services/user.service';

@NgModule({
    declarations: [
        AppComponent,
        SimpleTinyComponent,
        StoreComponent,
        ParksComponent,
        AnimalsComponent,
        ContactComponent,
        KeeperComponent,
        HomeComponent,
        NavComponent,
        ChurrosComponent,
        LoginComponent,
        RegisterComponent,
    ],
    imports: [
        BrowserModule,
        FormsModule,
        appRouting,
        EmailModule,
        BrowserAnimationsModule,
        AdminModule,
        HttpModule
    ],
    providers: [
        appRoutingProviders,
        UserService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
