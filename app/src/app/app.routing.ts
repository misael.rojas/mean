import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

//components
import { StoreComponent } from './components/store/store.component';
import { HomeComponent } from './components/home/home.component';
import { AnimalsComponent } from './components/animals/animals.component';
import { KeeperComponent } from './components/keeper/keeper.component';
import { ContactComponent } from './components/contact/contact.component';

import { ChurrosComponent } from './components/churros/churros.component';
import {LoginComponent} from './components/login/login.component';
import {RegisterComponent} from './components/register/register.component';

const appRoutes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'home', component: HomeComponent },
    { path: 'animals', component: AnimalsComponent },
    { path: 'keepers', component: KeeperComponent },
    { path: 'contact', component: ContactComponent },
    { path: 'store', component: StoreComponent },
    { path: 'churros', component: ChurrosComponent },
    { path: '**', pathMatch: 'full', redirectTo: 'home' }
];

export const appRoutingProviders: any[] = [];
//export const appRouting = RouterModule.forRoot(appRoutes);
export const appRouting: ModuleWithProviders = RouterModule.forRoot(appRoutes);
