import { Component, OnInit } from '@angular/core';
import { trigger, state, style, transition, animate} from '@angular/core';
import { fadeIn } from '../shared/animations/fadein';

@Component({
    selector: 'app-churros',
    templateUrl: './churros.component.html',
    styleUrls: ['./churros.component.css'],
    animations: [
        fadeIn,
        trigger('focusBtn', [
            state('inactive', style({
                transform: 'scale(1)'
            })),
            state('active', style({
                transform: 'scale(1.1)',
                borderRadius: '20px',
                marginLeft: '3px',
                background: 'red'
            })),
            transition('inactive => active', animate('3s ease-in')),
            transition('active => inactive', animate('3s ease-out'))
        ]),

    ]
})
export class ChurrosComponent implements OnInit {

    public title: string;
    public nombreParque: string;
    public metrosCuadrados: number;
    public miParque: any;
    public state: string;

    constructor() {
        this.title = 'Store component';
        this.state= 'inactive';
    }

    ngOnInit() {
        $('#textjq').hide();
        $('#buttonjq').click(function(){
            console.log('toggle alert');
            $('#textjq').slideToggle();
        });

        $('#textdot').dotdotdot({});
    }

    mostrarNombre () {
        console.log(this.nombreParque);
    }

    verDatosParque(event) {
        console.log(event);
        this.miParque = event;
    }

    textChurroEditor(content) {
        console.log('text in the churro text editor', content);
    }

    toggleState() {
        if( this.state === 'inactive' )
            this.state = 'active';
        else
            this.state = 'inactive';
    }

}