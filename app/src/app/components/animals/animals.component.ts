import { Component, OnInit } from '@angular/core';
import { fadeIn } from '../shared/animations/fadein';

@Component({
    selector: 'app-animals',
    templateUrl: './animals.component.html',
    styleUrls: ['./animals.component.css'],
    animations: [ fadeIn ]

})
export class AnimalsComponent implements OnInit {

    constructor() { }

    ngOnInit() {
    }

}