import { Component, OnInit } from '@angular/core';
import { fadeIn } from '../shared/animations/fadein';

@Component({
    selector: 'app-contact',
    templateUrl: './contact.component.html',
    styleUrls: ['./contact.component.css'],
    animations: [fadeIn]
})
export class ContactComponent implements OnInit {

    constructor() { }

    ngOnInit() {
    }

}
