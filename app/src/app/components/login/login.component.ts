import { Component, OnInit } from '@angular/core';
import {fadeIn} from '../shared/animations/fadein';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    animations: [ fadeIn ]
})
export class LoginComponent implements OnInit {

    constructor(
        private _route: ActivatedRoute,
        private _router: Router
    ) { }

    ngOnInit() {

    }

}
