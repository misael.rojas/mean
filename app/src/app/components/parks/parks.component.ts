import { Component, Input, Output, EventEmitter, OnChanges, SimpleChanges, OnInit, OnDestroy } from '@angular/core';
import { fadeIn } from '../shared/animations/fadein';

@Component({
    selector: 'app-parks',
    templateUrl: './parks.component.html',
    styleUrls: ['./parks.component.css'],
    animations: [ fadeIn ]
})
export class ParksComponent implements OnChanges, OnInit, OnDestroy{

    @Input() nombre: string;
    @Input('metros_cuadrados') metros: number;
    public vegetacion: string;
    public abierto: boolean;

    @Output() pasameLosDatos = new EventEmitter();

    constructor() {
        this.nombre = 'Parque natural para caballos';
        this.metros = 450;
        this.vegetacion = 'Alta';
        this.abierto = true;
    }

    ngOnChanges( changes: SimpleChanges){

        console.log( changes );

    }

    ngOnInit(){
        console.log('Metodo OnInit  del componente hijo Lanzado');
    }

    ngOnDestroy(){
        console.log('Metodo OnDestroy se ha ejecutado');
    }

    emitirEvento(){
        this.pasameLosDatos.emit({
            'nombre' : this.nombre,
            'metros' : this.metros,
            'vegetacion' : this.vegetacion,
            'abierto' : this.abierto
        })
    }

}
