import { Component, OnInit } from '@angular/core';
import { fadeIn } from '../shared/animations/fadein';

@Component({
    selector: 'app-store',
    templateUrl: './store.component.html',
    styleUrls: ['./store.component.css'],
    animations: [ fadeIn ]
})
export class StoreComponent implements OnInit {

    public title: string;
    public nombreParque: string;
    public metrosCuadrados: number;
    public miParque: any;

    constructor() {
        this.title = 'Store component';
    }

    ngOnInit() {
        $('#textjq').hide();
        $('#buttonjq').click(function(){
            console.log('toggle alert');
            $('#textjq').slideToggle();
        });
    }

    mostrarNombre (){
        console.log(this.nombreParque);
    }

    verDatosParque(event){
        console.log(event)
        this.miParque = event;
    }

}
