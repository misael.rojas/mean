import { Component, OnInit } from '@angular/core';
import { fadeIn } from '../shared/animations/fadein';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css'],
    animations: [ fadeIn ]
})
export class HomeComponent implements OnInit {

    constructor() { }

    ngOnInit() {

    }

}