import { Component, OnInit } from '@angular/core';
import {fadeIn} from '../shared/animations/fadein';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User} from '../../models/user.model';
import { GLOBAL } from '../../services/global.service';
import { UserService } from '../../services/user.service';


@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    animations: [ fadeIn ]
})
export class RegisterComponent implements OnInit {

    public user: User;
    public msg;
    public ok;

    constructor(
       private _route: ActivatedRoute,
       private _router: Router,
       private _userService: UserService
    ) {
        this.user =  new User('', '', '', '', '', 'basic', '');
        this.ok = false;
    }

    ngOnInit() {
    }

    onSubmit(registerForm) {
        this.msg = '';
        this._userService.register( this.user ).subscribe(
            response => {
                console.log( response );

                if ( response.user._id ) {
                    this.user = response.user;
                    this.msg = 'The user ' + this.user.name + ' ' + this.user.surname + 'has been register succefully';
                    this.ok = true;
                    registerForm.reset();
                }else{
                    this.msg = 'The user couldn\'t be saved';
                    this.ok = false;
                }
            },
            error => {
                console.log(<any>error);
                this.msg = 'The user couldn\'t be saved';
                this.ok = false;
            }

        );
    }

}
