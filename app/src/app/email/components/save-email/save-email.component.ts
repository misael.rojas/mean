import { Component } from '@angular/core';

@Component({
  selector: 'app-save-email',
  templateUrl: './save-email.component.html'
})
export class SaveEmailComponent {

  contactEmail: string;

  constructor() { }

    saveEmail() {
    localStorage.setItem('contactEmail', this.contactEmail);
  }

}