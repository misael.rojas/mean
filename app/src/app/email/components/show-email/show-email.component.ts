import {Component, DoCheck, OnInit} from '@angular/core';

@Component({
  selector: 'app-show-email',
  templateUrl: './show-email.component.html',
  styles: []
})
export class ShowEmailComponent implements OnInit, DoCheck {

    contactEmail: string;

    constructor() { }

    ngOnInit() {
        this.contactEmail = localStorage.getItem('contactEmail');
    }

    ngDoCheck() {
        this.contactEmail = localStorage.getItem('contactEmail');
    }

    deleteEmail() {
        localStorage.removeItem('contactEmail');
        localStorage.clear();
        this.contactEmail = null;
    }

}
