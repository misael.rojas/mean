import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

//components
import { ListAnimalsComponent } from './components/list-animals/list-animals.component';
import { AddAnimalComponent } from './components/add-animal/add-animal.component';
import { EditAnimalComponent } from './components/edit-animal/edit-animal.component';
import { AdminMainComponent } from './admin-main/admin-main.component';

const adminRoutes: Routes = [
    {
        path: 'admin-panel',
        component: AdminMainComponent,
        children: [
            { path: 'list', component: ListAnimalsComponent },
            { path: 'add', component: AddAnimalComponent },
            { path: 'edit', component: EditAnimalComponent },
            { path: '', redirectTo: 'list', pathMatch: 'full' }
        ]
    }
];

@NgModule({
  imports: [
    RouterModule.forChild(adminRoutes)
  ],
  exports: [
      RouterModule
  ]
})

export class AdminRoutingModule { }
