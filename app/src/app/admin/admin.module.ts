import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import { AdminRoutingModule } from './admin-routing.module';

//components
import {ListAnimalsComponent} from './components/list-animals/list-animals.component';
import {AddAnimalComponent} from './components/add-animal/add-animal.component';
import {EditAnimalComponent} from './components/edit-animal/edit-animal.component';
import {AdminMainComponent} from './admin-main/admin-main.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        HttpModule,
        AdminRoutingModule
    ],
    declarations: [
        ListAnimalsComponent,
        AddAnimalComponent,
        EditAnimalComponent,
        AdminMainComponent
    ],
    exports: [],
    providers: []
})
export class AdminModule { }
