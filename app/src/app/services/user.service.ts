import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable} from 'rxjs/Observable';
import { GLOBAL } from './global.service';

@Injectable()
export class UserService{
    public url: string;

    constructor(private _http: Http) {
        this.url = GLOBAL.api;
    }

    register(user) {
        let params = JSON.stringify( user );
        const headers = new Headers({'content-Type': 'application/json'});

        return this._http.post(this.url + 'register', params, {headers: headers})
            .map(res => res.json());

    }

}
