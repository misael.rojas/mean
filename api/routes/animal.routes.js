'use strict';

var express = require('express');
var AnimalsController = require('../controllers/animals.controllers');
var global = require('../global');

var api = express.Router();

var authMd = require('../middlewares/authenticated');
var isAdminMd = require('../middlewares/is-admin');

var multipart = require('connect-multiparty');
var mp_upload = multipart({ 'uploadDir': global.site.animals_images_dir});


api.get('/animals/testing', authMd.isAuthenticated, AnimalsController.testing);

api.post('/animals', [authMd.isAuthenticated, isAdminMd.isAdmin], AnimalsController.save);
api.get('/animals', AnimalsController.getAnimals);
api.get('/animals/:id', AnimalsController.getAnimal);
api.put('/animals/:id', [authMd.isAuthenticated, isAdminMd.isAdmin], AnimalsController.update);
api.delete('/animals/:id', [authMd.isAuthenticated, isAdminMd.isAdmin], AnimalsController.deleteAnimal);
api.post( '/animals/upload-image/:id', [authMd.isAuthenticated, mp_upload], AnimalsController.uploadImage );
api.get( '/animals/get-image/:file', [authMd.isAuthenticated], AnimalsController.getImageFile );

module.exports = api;