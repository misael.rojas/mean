'use strict';

var express = require('express');
var UserController = require('../controllers/user.controller');
var global = require('../global');

var api = express.Router();

var authMd = require('../middlewares/authenticated');

var multipart = require('connect-multiparty');
var mp_upload = multipart({ 'uploadDir': global.site.user_images_dir});


api.get('/testing', authMd.isAuthenticated, UserController.testing);
api.post('/register', UserController.save);
api.post('/login', UserController.login);
api.put( '/user/:id', authMd.isAuthenticated, UserController.update );
api.post( '/user/upload-image/:id', [authMd.isAuthenticated, mp_upload], UserController.uploadImage );
api.get( '/user/get-image/:file', [authMd.isAuthenticated], UserController.getImageFile );
api.get('/users/keepers', UserController.getKeepers);

module.exports = api;