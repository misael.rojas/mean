'use strict';

var mongoose = require('mongoose');
var app = require('./app');
var port = process.env.PORT || 3789;

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/ngzoo', { useMongoClient: true })
    .then(
        () => {
            console.log('la connexion a la base de datos ngzoo se a realiazdo exitosamente');

            app.listen(port, () => {
                console.log('El servidor local con Node y Express está corriendo correctamente...');
            })

        }
    ).catch(
        err => console.log(err)
    );