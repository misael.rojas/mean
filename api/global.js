var config = {
    db: {
        host: '127.0.0.1',
        db: 'ngzoo',
        /*user: 'root',
        password: '123456',
        database: 'codejobs',
        debug: true,
        //socket: '/var/run/mysqld/mysqld.sock', // For linux...
        socket: '/Applications/XAMPP/xamppfiles/var/mysql/mysql.sock' //For mac...
        */
    },
    site: {
        url: 'http://localhost:3000',
        title: 'NGZoo',
        language: 'en',
        user_images_dir: './uploads/users/images',
        animals_images_dir: './uploads/animals/images'
    },
    secret: 'clave_super_secreta_del_curso_de_angular4avanzado'
};

module.exports = config;