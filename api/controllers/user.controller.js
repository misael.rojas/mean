'use strict';
//modulos
var bcrypt = require('bcrypt-nodejs');
var fs = require('fs');
var path = require('path');

//models
var User = require('../models/user.model');

//services
var jwtService = require('../services/jwt.service');
var global = require('../global');

function testing(req, res){
    res.status(200).send({
        message: 'testing the user controller and the action test',
        user: req.authUser
    });
}

function save(req, res){

    var user = new User();

    var params = req.body;

    console.log(params.name);
    console.log(params.surname);
    console.log(params.email);
    console.log(params.password);
    if(params.name && params.surname && params.email && params.password) {
        user.name = params.name;
        user.surname = params.surname;
        user.email = params.email.toLowerCase();
        user.role = 'basic';
        user.image = null;

        User.findOne({ email: params.email.toLowerCase() }, (err, issetUser) => {
            if(err){
                res.status(500).send({message: 'Error to check the user'});
            }else{
                if(!issetUser){
                    bcrypt.hash(params.password, null, null, function(err, hash){
                        user.password = hash;
                        user.save( (err, userStored) => {
                            if(err){
                                res.status(500).send({message: 'Error when save user'});
                            }else{
                                if(!userStored){
                                    res.status(404).send({message: 'The user hasn\'t been register'});
                                }else{
                                    res.status(200).send({user: userStored});
                                }
                            }
                        });
                    })
                }else{
                    res.status(200).send({user: 'The user counln\'t be register because the email already exist'});
                }
            }
        });
        //if()
    }else{
        res.status(200).send({ message: 'Please, send all required data and try again' });
    }
}

function login(req, res) {

    var params = req.body;
    var email = params.email.toLowerCase();
    var password = params.password;

    User.findOne({ email: email }, (err, user) => {
        if(err){
            res.status(500).send({message: 'Error to check the user'});
        }else{

            if(user){
                bcrypt.compare(password, user.password, function(err, check) {
                    if(check){
                        if(params.gettoken){
                            //deveolver token jwt
                            res.status(200).send({
                                token: jwtService.createToken( user )
                            });
                        }else{
                            res.status(200).send({ user });

                        }

                    }else{
                        res.status(404).send({ message: 'The user couldn\'t login, wrong email or password' });
                    }
                });
            }else{
                res.status(404).send({ message: 'User not found'});
            }
        }
    });
}

function update(req, res) {
    var userId = req.params.id;
    var update = req.body;

    if(userId != req.authUser.sub) {
        return res.status(500).send({message: 'Permision denied to update this user',});
    }

    User.findByIdAndUpdate (userId, update, { new: true }, (err, userUpdated) => {
        if(err) {
            return res.status(500).send({message: 'error to update the user',});
        }else{
            if(!userUpdated) {
                return res.status(404).send({message: 'The user couln\'t been updated',});
            }else{
                return res.status(404).send({message: 'user updated', user: userUpdated});
            }
        }
    })
}

function uploadImage(req, res) {
    var userId = req.params.id;
    var fileName = 'No uploaded';

    if(userId != req.authUser.sub) {
        return res.status(500).send({message: 'Permision denied to update this user'});
    }

    if(req.files) {
        var filePath = req.files.image.path;
        var fileSplit = filePath.split('/');
        var fileName = fileSplit[3];

        var extSplit = fileName.split('.');
        var ext = extSplit[1];
        var exts = ['jpg', 'png', 'gif', 'jpeg'];


        if(exts.indexOf(ext) <= -1) {
            fs.unlink(filePath, (err) => {
               if(err) {
                   return res.status(200).send({message: 'extention file not valid and file couldn\t be deleted'});
               }else{
                   return res.status(200).send({message: 'extention file not valid'});

               }
            });
        }else{
            User.findByIdAndUpdate (userId, { image: fileName }, { new: true }, (err, userUpdated) => {
                if(err) {
                    return res.status(500).send({message: 'error to update the user',});
                }else{
                    if(!userUpdated) {
                        return res.status(404).send({message: 'The user couln\'t been updated'});
                    }else{
                        return res.status(404).send({message: 'user updated', user: userUpdated, image: fileName });
                    }
                }
            })
        }

       /* res.status(200).send({
            file_name: fileName,
            file_path: filePath,
            file_split: fileSplit,
            ext: ext,
        })*/
    }else{
        return res.status(200).send({message: 'The file couldn\'t be updated',});
    }
}

function getImageFile(req, res) {
    var imageFile = req.params.file;
    var pathFile = global.site.user_images_dir + '/' + imageFile;

    console.log(imageFile);

    fs.exists(pathFile, function(exists){
        if(exists){
            res.sendFile(path.resolve(pathFile));
        }else{
            return res.status(404).send({message: 'The file doesn\'t exits',});

        }
    })
}

function getKeepers(req, res) {
    User.find({role: 'admin'}).exec((err, users) => {
        if(err){
            res.status(500).send({message: 'Error in the request'});
        }else{
            if(!users.length){
                res.status(404).send({message: 'There are not keepers'});
            }else{
                res.status(200).send({keepers: users});
            }
        }
    });
}

module.exports = {
    testing,
    save,
    login,
    update,
    uploadImage,
    getImageFile,
    getKeepers
};