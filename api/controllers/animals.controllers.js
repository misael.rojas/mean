'use strict';
//modulos
var bcrypt = require('bcrypt-nodejs');
var fs = require('fs');
var path = require('path');

//models
var Animal = require('../models/animal.model');

//services
var jwtService = require('../services/jwt.service');
var global = require('../global');

function testing(req, res){
    res.status(200).send({
        message: 'testing the animals controller and the action test',
        user: req.authUser
    });
}

function getAnimals(req, res) {
    Animal.find({}).populate({path: 'user'}).exec((err, animals) => {
        if(err){
            res.status(500).send({message: 'Error in the request'});
        }else{
            if(!animals.length){
                res.status(404).send({message: 'There are not animals'});
            }else{
                res.status(200).send({ animals: animals });
            }
        }
    });

}

function getAnimal(req, res) {
    var animalId = req.params.id;

    Animal.findById (animalId).populate({ path: 'user'}).exec( (err, animal) => {
        if(err) {
            return res.status(500).send({message: 'error to get the animal',});
        }else{
            if(!animal) {
                return res.status(404).send({message: 'The animal couln\'t be found',});
            }else{
                return res.status(404).send({ animal });
            }
        }
    })

}

function update(req, res) {
    var animalId = req.params.id;
    var update = req.body;

    Animal.findByIdAndUpdate (animalId, update, { new: true }, (err, animalUpdated) => {
        if(err) {
            return res.status(500).send({message: 'error to update the animal',});
        }else{
            if(!animalUpdated) {
                return res.status(404).send({message: 'The animal couln\'t be updated',});
            }else{
                return res.status(404).send({message: 'animal updated', animal: animalUpdated});
            }
        }
    })

}

function deleteAnimal(req, res) {
    var animalId = req.params.id;

    Animal.findByIdAndRemove (animalId, (err, animalRemoved) => {
        if(err) {
            return res.status(500).send({message: 'error to remove the animal',});
        }else{
            if(!animalRemoved) {
                return res.status(404).send({message: 'The animal couln\'t be remove',});
            }else{
                return res.status(404).send({message: 'animal removed', animal: animalRemoved});
            }
        }
    })

}

function save(req, res){

    var animal = new Animal();

    var params = req.body;

    if(params.name) {
        animal.name = params.name;
        animal.description = params.description;
        animal.year = params.year;
        animal.image = null;

        animal.user = req.authUser.sub;

        animal.save( (err, animalStored) => {
            if(err){
                res.status(500).send({message: 'Error when save the animal'});
            }else{
                if(!animalStored){
                    res.status(404).send({message: 'The animal hasn\'t been saved'});
                }else{
                    res.status(200).send({ animal: animalStored });
                }
            }
        });
        //if()
    }else{
        res.status(200).send({ message: 'Please, send all required data and try again' });
    }
}

function uploadImage(req, res) {
    var animalId = req.params.id;
    var fileName = 'No uploaded';

    if(req.files) {
        var filePath = req.files.image.path;
        var fileSplit = filePath.split('/');
        var fileName = fileSplit[3];

        var extSplit = fileName.split('.');
        var ext = extSplit[1];
        var exts = ['jpg', 'png', 'gif', 'jpeg'];


        if(exts.indexOf(ext) <= -1) {
            fs.unlink(filePath, (err) => {
                if(err) {
                    return res.status(200).send({message: 'extention file not valid and file couldn\t be deleted'});
                }else{
                    return res.status(200).send({message: 'extention file not valid'});

                }
            });
        }else{
            Animal.findByIdAndUpdate (animalId, { image: fileName }, { new: true }, (err, animalUpdated) => {
                if(err) {
                    return res.status(500).send({message: 'error to update the animal',});
                }else{
                    if(!animalUpdated) {
                        return res.status(404).send({message: 'The animal couln\'t be updated'});
                    }else{
                        return res.status(404).send({message: 'animal updated', animal: animalUpdated, image: fileName });
                    }
                }
            })
        }

        /* res.status(200).send({
             file_name: fileName,
             file_path: filePath,
             file_split: fileSplit,
             ext: ext,
         })*/
    }else{
        return res.status(200).send({message: 'The file couldn\'t be updated',});
    }
}

function getImageFile(req, res) {
    var imageFile = req.params.file;
    var pathFile = global.site.animals_images_dir + '/' + imageFile;

    fs.exists(pathFile, function(exists){
        if(exists){
            res.sendFile(path.resolve(pathFile));
        }else{
            return res.status(404).send({message: 'The file doesn\'t exits',});

        }
    })
}


module.exports = {
    testing,
    save,
    getAnimals,
    getAnimal,
    update,
    uploadImage,
    getImageFile,
    deleteAnimal
};